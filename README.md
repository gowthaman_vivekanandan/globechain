# globechain

Steps to install:

Using bash script:

1. Clone the repository
2. Change the working directory to repository
3. bash setup.sh # This will install and start postgres and web server

Using docker:

1. Clone the repository
2. Change the working directory to repository
3. Pull the docker images docker pull registry.gitlab.com/gowthaman_vivekanandan/globechain and docker pull registry.gitlab.com/gowthaman_vivekanandan/postgres
4. docker-compose up



API's:

Create User:

mutation {
  createUser(username : "Gowthaman", password : "password", location : "{lat:-12,lng:151}",accountType : "Charity", firstName : "Gowthaman", lastName : "V", fullAddress : "Stonecrop place") {
    user {
      id
    }
  }
}


Login:

mutation {
 login(username : "gowthaman", password : "password") {
  token
}    
}

Get Profile:

{
  me {
      firstName,
      lastName,
      fullAddress,
      accountType,
      location
    user {
        username,
        id
    }
  }
}
