#!/usr/bin/env bash
ssh -v -o StrictHostKeyChecking=no deploy@3.128.32.203 << 'ENDSSH'
 cd /home/deploy/globechain
 sudo docker login -u $REGISTRY_USER -p $CI_BUILD_TOKEN $CI_REGISTRY
 sudo docker pull registry.gitlab.com/gowthaman_vivekanandan/globechain:latest
 sudo docker pull registry.gitlab.com/gowthaman_vivekanandan/postgres:latest
 screen -d -m -S deploy bash -c 'sudo docker-compose up'
ENDSSH
